#!/usr/bin/env python3

# Variables defined outside any function are called global variables.
a = 1

def increase_a(a):
    """ Increase the value of the argument by 1"""
    a = a + 1
    return a

def main():

    a = 10
    a = increase_a(a)
    # Can you guess what will be output here?
    print(a)

if __name__ == '__main__':
    a = 20
    main()
